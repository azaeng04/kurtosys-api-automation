const { kurtosysUrl } = require('../lib/urls');
const { httpGet } = require('../lib/http-helper');
const { req } = require('../fixtures/fixture');

describe('API Kurtosys Tests', () => {
    test('can return a http 200 status code', async () => {
        const url = kurtosysUrl({ dataVersion: '1562843391575', req });

        const response = await httpGet(url.uri, url.qs);

        expect(response.statusCode).toEqual(200);
    });

    test('can verify properties_pub object exists', async () => {
      const url = kurtosysUrl({ dataVersion: '1562843391575', req });

      const response = await httpGet(url.uri, url.qs);
      const { body } = response;
      const { values } = body;

      expect(values).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            properties_pub: expect.anything()
          })
        ])
      );
    });

    test('can verify that the properties_pub object has a base_currency property', async () => {
      const url = kurtosysUrl({ dataVersion: '1562843391575', req });

      const response = await httpGet(url.uri, url.qs);
      const { body } = response;
      const { values } = body;

      expect(values).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            properties_pub: expect.objectContaining({
              base_currency: expect.anything()
            }),
          })
        ])
      );
    });

    test('can verify response time api request', async () => {
      const url = kurtosysUrl({ dataVersion: '1562843391575', req });

      const response = await httpGet(url.uri, url.qs);

      console.log(`Response time in ms: ${response.elapsedTime}`);
    });
    
    test('can verify size of response', async () => {
      const url = kurtosysUrl({ dataVersion: '1562843391575', req });

      const response = await httpGet(url.uri, url.qs);

       console.log(`Size of the response ${response.headers['content-length}']}`);
    });
});