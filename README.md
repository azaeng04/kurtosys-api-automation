
# Kurtosys API Assessment
This project is an assessment answering questions around the Kurtosys API

> How to execute tests

Requirements
This assessment assumes you are running on a Linux based OS with the following items installed:

 - Git
 - Docker
 - Docker-compose
 - Nodejs 12 and up

Execute the following instructions:
1. Perform a `git clone git@gitlab.com:azaeng04/kurtosys-api-automation.git`
2. Navigate to `cd kurtosys-api-automation`
3. Perform a `docker-compose run --rm tests npm ci`, if that throws an error then execute `docker-compose run --rm tests npm install`
4. Run the tests with `docker-compose run --rm tests npm test`