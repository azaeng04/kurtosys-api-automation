const { hosts } = require('./hosts');

const kurtosysUrl = ({ dataVersion, req }) => ({
    uri: `${hosts.apiKurtosys}/fund/searchentity`,
    qs: { 
        'data-version': dataVersion,
        req,
    },
});

module.exports = {
    kurtosysUrl,
}
