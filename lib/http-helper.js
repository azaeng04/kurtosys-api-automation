const rp = require('request-promise-native');

const defaultHeaders = {
    Accept: 'application/json',
    'Cache-Control': 'no-cache, no-store',
};

async function httpGet(url, queryStringParams) {
    const response = await rp({
        time: true,
        method: 'GET',
        uri: url,
        qs: queryStringParams,
        headers: defaultHeaders,
        resolveWithFullResponse: true,
        strictSSL: false,
        json: true,
    }).catch(error => {
        return error;
    });
    return response;
}

module.exports = {
    httpGet,
}
