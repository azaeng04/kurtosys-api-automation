const { env, environmentVariable } = require('./env');

function processEnv(name) {
    return (process.env[name] === undefined) ? environmentVariable[env][name] : process.env[name];
}

const apiKurtosysProtocol = processEnv('HTTP_SCHEMA');
const apiKurtosysHost = processEnv('API_KURTOSYS');

const hosts = {
    apiKurtosys: `${apiKurtosysProtocol}://${apiKurtosysHost}/tools/ksys373/api`
};

module.exports = {
    hosts,
};
