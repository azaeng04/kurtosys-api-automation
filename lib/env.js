let env = process.env.TEST_ENV;
env = (env === undefined) ? 'production': env;

function hosts() {
    return {
        kurtosysApi: `api.kurtosys.io`
    }
}

const environmentVariable = {
    production: {
        HTTP_SCHEMA: 'https',
        API_KURTOSYS: hosts().kurtosysApi,
    }
}

module.exports = {
    env,
    environmentVariable,
};
