const Joi = require('@hapi/joi');

async function validateContract(response, contract) {
    if(!response || !contract) throw new Error('Must pass in a response and contract');
    const options = { abortEarly: false, presence: 'required', allowUnkown: true };
    Joi.valid(response, contract, options, error => {
        if(error) {
            Joi.assert(response, contract);
        }
    });
}

module.exports = {
    validateContract,
}
