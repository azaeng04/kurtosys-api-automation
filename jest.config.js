
const TEST_CONFIG = {
    testRegex: `${__dirname}/__tests__/.*\\.test.js`,
    testPathIgnorePatterns: ['/node_modules/'],
    setupFilesAfterEnv: ['<rootDir>/jest.setup.js']
};

module.exports = TEST_CONFIG;
